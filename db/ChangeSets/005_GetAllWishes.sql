#liquibase formatted sql
#changeset reid:5
CREATE PROCEDURE `sp_GetAllWishes`()
BEGIN
    select wish_id,wish_title,wish_description,wish_file_path from tbl_wish;
END