#liquibase formatted sql
#changeset reid:3
CREATE PROCEDURE `sp_addWish`(
    IN p_title varchar(45),
    IN p_description varchar(1000),
    IN p_user_id bigint,
    IN p_file_path varchar(200)
)
BEGIN
    insert into tbl_wish(
        wish_title,
        wish_description,
        wish_user_id,
        wish_date,
        wish_file_path
    )
    values
    (
        p_title,
        p_description,
        p_user_id,
        NOW(),
        p_file_path
    );
END