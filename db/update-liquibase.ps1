
$errorLog = $("./liquibase.log")
New-Item -ItemType Directory -Force -Path C:\Temp | Out-Null
$changeLogFile = Join-Path (Get-Item -Path ".\" -Verbose).FullName $changeLogFileName
$template = 'liquibase --driver=com.mysql.jdbc.Driver --classpath=".\mysql-connector-java-5.1.40-bin.jar" --changeLogFile="Changelog.xml" --url="jdbc:mysql://localhost/bucketlist" --username="bucketlist_user" --password="password" update >> {0}'
$here = $template -f $errorLog
& cmd.exe /c $here